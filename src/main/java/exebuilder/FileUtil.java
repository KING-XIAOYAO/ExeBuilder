package exebuilder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

public class FileUtil {

  public static void deleteDir(String absPath) throws IOException {
    // 清空文件夹
    // Files.walk - return all files/directories below rootPath including
    // .sorted - sort the list in reverse order, so the directory itself comes after the including
    // subdirectories and files
    // .map - map the Path to File
    // .peek - is there only to show which entry is processed
    // .forEach - calls the .delete() method on every File object
    Files.walk(Paths.get(absPath)).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
  }
}
